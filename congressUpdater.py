#!/usr/bin/env

#################
# Congress Updater
# Set-up for 113th
#################

import sys
import os
import json
import re
import pprint
import csv
import logging
import math
import itertools
import MySQLdb
import re
import pickle
from collections import OrderedDict
from operator import itemgetter
import datetime

path1 = "/data/congress/"
os.chdir(path1)
db = MySQLdb.connect(read_default_file="/etc/mysql/my.cnf",db="legex")
execfile("/data/congress/LegExCode/legex-data/actionParser.py")
execfile("/data/congress/LegExCode/legex-data/binfoParser.py")

print "loading member info"
allM = pickle.load( open("/data/congress/data/allM.p", "rb" ) )
bigDex = ['-'.join((str(r['thomas']),str(r['cong']))) for r in allM]

## un comment one of these lines to run. First line will allow it to be run from command line.
#cong = sys.argv[1]
cong = 113


congress= cong
congt = str(cong)
print "starting "+ congt

# create list of all bills/res for a given congress by type
hr = [filename for filename in os.listdir(path1+"data/"+str(congress)+"/bills/hr/") if re.search(r'hr\d{1,4}', filename) != None]
hconres = [filename for filename in os.listdir(path1+"data/"+str(congress)+"/bills/hconres/") if re.search(r'hconres\d{1,4}', filename) != None]
hjres = [filename for filename in os.listdir(path1+"data/"+str(congress)+"/bills/hjres/") if re.search(r'hjres\d{1,4}', filename) != None]
hres = [filename for filename in os.listdir(path1+"data/"+str(congress)+"/bills/hres/") if re.search(r'hres\d{1,4}', filename) != None]

s = [filename for filename in os.listdir(path1+"data/"+str(congress)+"/bills/s/") if re.search(r's\d{1,4}', filename) != None]
sconres = [filename for filename in os.listdir(path1+"data/"+str(congress)+"/bills/sconres/") if re.search(r'sconres\d{1,4}', filename) != None]
sjres = [filename for filename in os.listdir(path1+"data/"+str(congress)+"/bills/sjres/") if re.search(r'sjres\d{1,4}', filename) != None]
sres = [filename for filename in os.listdir(path1+"data/"+str(congress)+"/bills/sres/") if re.search(r'sres\d{1,4}', filename) != None]



# Look up idOLD
cursor0 = db.cursor(MySQLdb.cursors.DictCursor)
cursor0.execute('SELECT id,BillNum,BillType FROM cbp_main.bills WHERE Cong=%s', (cong))
bIDs = cursor0.fetchall()
cursor0.close()
bIDsOLD = [r['id'] for r in bIDs]
billDex0 = [''.join((str(r['BillType']),str(r['BillNum']))) for r in bIDs]
#bid = bIDsOLD[billDex0.index(''.join((billtype.upper(),str(bill))))]


# Look up idNEW
cursor1 = db.cursor(MySQLdb.cursors.DictCursor)
x = cursor1.execute('SELECT idNEW,BillNum,BillType FROM billsNEW WHERE Cong=%s', (cong))
bIDs2 = cursor1.fetchall()
cursor1.close()
bIDsNEW = [r['idNEW'] for r in bIDs2]
billDex1 = [''.join((str(r['BillType']),str(r['BillNum']))) for r in bIDs2]
#bid = bIDsOLD[billDex0.index(''.join((billtype.upper(),str(bill))))]

newBills = []
updBills = []

print "parsing hr"
for b in hr:
	try:
		bidNEW = bIDsNEW[billDex1.index(b)]
		NEW = 0
	except ValueError:
		NEW = 1
	if NEW == 1:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'hr',bid,path1)
		print "NEW: " + b
		newBills.append(bill)
	if NEW==0:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'hr',bid,path1)
		bill['idNEW'] = bidNEW
		print "UPDATED: " + b
		updBills.append(bill)


print "parsing hconres"
for b in hconres:
	try:
		bidNEW = bIDsNEW[billDex1.index(b)]
		NEW = 0
	except ValueError:
		NEW = 1
	if NEW == 1:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'hconres',bid,path1)
		print "NEW: " + b
		newBills.append(bill)
	if NEW==0:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'hconres',bid,path1)
		bill['idNEW'] = bidNEW
		print "UPDATED: " + b
		updBills.append(bill)

print "parsing hjres"
for b in hjres:
	try:
		bidNEW = bIDsNEW[billDex1.index(b)]
		NEW = 0
	except ValueError:
		NEW = 1
	if NEW == 1:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'hjres',bid,path1)
		print "NEW: " + b
		newBills.append(bill)
	if NEW==0:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'hjres',bid,path1)
		bill['idNEW'] = bidNEW
		print "UPDATED: " + b
		updBills.append(bill)

print "parsing hres"
for b in hres:
	try:
		bidNEW = bIDsNEW[billDex1.index(b)]
		NEW = 0
	except ValueError:
		NEW = 1
	if NEW == 1:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'hres',bid,path1)
		print "NEW: " + b
		newBills.append(bill)
	if NEW==0:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'hres',bid,path1)
		bill['idNEW'] = bidNEW
		print "UPDATED: " + b
		updBills.append(bill)

print "parsing s"
for b in s:
	try:
		bidNEW = bIDsNEW[billDex1.index(b)]
		NEW = 0
	except ValueError:
		NEW = 1
	if NEW == 1:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'s',bid,path1)
		print "NEW: " + b
		newBills.append(bill)
	if NEW==0:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'s',bid,path1)
		bill['idNEW'] = bidNEW
		print "UPDATED: " + b
		updBills.append(bill)

print "parsing sconres"
for b in sconres:
	try:
		bidNEW = bIDsNEW[billDex1.index(b)]
		NEW = 0
	except ValueError:
		NEW = 1
	if NEW == 1:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'sconres',bid,path1)
		print "NEW: " + b
		newBills.append(bill)
	if NEW==0:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'sconres',bid,path1)
		bill['idNEW'] = bidNEW
		print "UPDATED: " + b
		updBills.append(bill)

print "parsing sjres"
for b in sjres:
	try:
		bidNEW = bIDsNEW[billDex1.index(b)]
		NEW = 0
	except ValueError:
		NEW = 1
	if NEW == 1:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'sjres',bid,path1)
		print "NEW: " + b
		newBills.append(bill)
	if NEW==0:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'sjres',bid,path1)
		bill['idNEW'] = bidNEW
		print "UPDATED: " + b
		updBills.append(bill)

print "parsing sres"
for b in sres:
	try:
		bidNEW = bIDsNEW[billDex1.index(b)]
		NEW = 0
	except ValueError:
		NEW = 1
	if NEW == 1:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'sres',bid,path1)
		print "NEW: " + b
		newBills.append(bill)
	if NEW==0:
		try:
			bid = bIDsOLD[billDex0.index(b.upper())]
		except ValueError:
			bid = 0
		bill = binfoParser(re.sub("[^0-9]", "",b),congt,'sres',bid,path1)
		bill['idNEW'] = bidNEW
		print "UPDATED: " + b
		updBills.append(bill)

billINS = "INSERT INTO billsNEW (BillNum,BillType,Cong,IntrDate,ShortTitle,OfficialTitle,PopTitle,SpThomasID,SpName,SpState,SpDist,UpdatedAt,CoSpThID,PLawNo,idOLD,ChRef,RankRef,MemRef) VALUES (%(BNum)s,%(BillType)s,%(Cong)s,%(Intro)s,%(ShortTitle)s,%(OfficialTitle)s,%(PopTitle)s,%(SpThomasID)s,%(SpName)s,%(SpState)s,%(SpDist)s,%(UpdatedAt)s,%(CoSpThID)s,%(PLawNo)s,%(billID)s,%(ChRef)s,%(RankRef)s,%(MemRef)s);"
billUPD = "UPDATE billsNEW SET ShortTitle = %(ShortTitle)s, OfficialTitle = %(OfficialTitle)s,PopTitle = %(PopTitle)s,CoSpThID = %(CoSpThID)s,PLawNo = %(PLawNo)s WHERE  idNew = %(idNEW)s;"

print "uploading NEW bills to MySQL"
a = 0
for k in newBills:
	#print a
	a +=1
	if 'CoSpThID' in k:
		k['CoSpThID'] = ','.join(k['CoSpThID'])
	else:
		k['CoSpThID'] = ''
	if 'PLawNo' not in k:
		k['PLawNo'] = ''
	cursor2 = db.cursor()
	k = cursor2.execute(billINS, k) # write bill characteristics to db
	cursor2.close()

db.commit()

print "UPDATING bills"
a = 0
for k in updBills:
	#print a
	a +=1
	if 'CoSpThID' in k:
		k['CoSpThID'] = ','.join(k['CoSpThID'])
	else:
		k['CoSpThID'] = ''
	if 'PLawNo' not in k:
		k['PLawNo'] = ''
	cursor2 = db.cursor()
	k = cursor2.execute(billUPD, k) # write bill characteristics to db
	cursor2.close()

db.commit()


#switching to ACTIONS
print "Now time for actions."
db = MySQLdb.connect(read_default_file="/etc/mysql/my.cnf",db="legex")


# Look up CBP ID
cursor1 = db.cursor(MySQLdb.cursors.DictCursor)
z = cursor1.execute('SELECT idNEW,BillNum,BillType FROM billsNEW WHERE Cong=%s', (cong))
#cursor1 = db.cursor() #looks up the bill number from CBP bills table.
#cursor1.execute('SELECT idNEW FROM billsNEW WHERE BillNum=%s AND Cong=%s AND BillType=%s', (bill, cong, billtype.upper()))
bIDs = cursor1.fetchall()
cursor1.close()
bIDsL = [r['idNEW'] for r in bIDs]
billDex = [''.join((str(r['BillType']),str(r['BillNum']))) for r in bIDs]
#billID = bIDsL[billDex.index(''.join((billtype,str(bill))))]

bigMoves = []
print "parsing hr actions"
for b in hr:
	 #print b
	 bid = bIDsL[billDex.index(''.join(('hr',re.sub("[^0-9]", "",b))))]
	 moves = actionParser(re.sub("[^0-9]", "",b),congt,'hr',bid,path1)
	 bigMoves.append(moves)

print "parsing hconres actions"
for b in hconres:
	 #print b
	 bid = bIDsL[billDex.index(''.join(('hconres',re.sub("[^0-9]", "",b))))]
	 moves = actionParser(re.sub("[^0-9]", "",b),congt,'hconres',bid,path1)
	 bigMoves.append(moves)

print "parsing hrjres actions"
for b in hjres:
	 #print b
	 bid = bIDsL[billDex.index(''.join(('hjres',re.sub("[^0-9]", "",b))))]
	 moves = actionParser(re.sub("[^0-9]", "",b),congt,'hjres',bid,path1)
	 bigMoves.append(moves)

print "parsing hres actions"
for b in hres:
	 #print b
	 bid = bIDsL[billDex.index(''.join(('hres',re.sub("[^0-9]", "",b))))]
	 moves = actionParser(re.sub("[^0-9]", "",b),congt,'hres',bid,path1)
	 bigMoves.append(moves)

print "parsing s actions"
for b in s:
	 #print b
	 bid = bIDsL[billDex.index(''.join(('s',re.sub("[^0-9]", "",b))))]
	 moves = actionParser(re.sub("[^0-9]", "",b),congt,'s',bid,path1)
	 bigMoves.append(moves)

print "parsing sconres actions"
for b in sconres:
	 #print b
	 bid = bIDsL[billDex.index(''.join(('sconres',re.sub("[^0-9]", "",b))))]
	 moves = actionParser(re.sub("[^0-9]", "",b),congt,'sconres',bid,path1)
	 bigMoves.append(moves)

print "parsing sjres actions"
for b in sjres:
	 #print b
	 bid = bIDsL[billDex.index(''.join(('sjres',re.sub("[^0-9]", "",b))))]
	 moves = actionParser(re.sub("[^0-9]", "",b),congt,'sjres',bid,path1)
	 bigMoves.append(moves)

print "parsing sres actions"
for b in sres:
	 #print b
	 bid = bIDsL[billDex.index(''.join(('sres',re.sub("[^0-9]", "",b))))]
	 moves = actionParser(re.sub("[^0-9]", "",b),congt,'sres',bid,path1)
	 bigMoves.append(moves)



# Look up action count per bill
cursor1 = db.cursor(MySQLdb.cursors.DictCursor)
z = cursor1.execute('SELECT COUNT(*) AS `Rows`, `billID` FROM `actions` WHERE cong = %s GROUP BY `billID` ORDER BY `billID`', (cong))
aCounts = cursor1.fetchall()
cursor1.close()
abIDs = [r['billID'] for r in aCounts]

actINS = "INSERT INTO actions (billID,bill,cong,billtype,acted_at,loc,status,actno,subbill) VALUES (%(billID)s,%(bill)s,%(cong)s,%(billtype)s,%(acted_at)s,%(loc)s,%(status)s,%(actno)s,%(subbill)s);"
actINS2 = "INSERT INTO actions (billID,bill,cong,billtype,acted_at,loc,status,actno,subbill,manUpdate) VALUES (%(billID)s,%(bill)s,%(cong)s,%(billtype)s,%(acted_at)s,%(loc)s,%(status)s,%(actno)s,%(subbill)s,%(manUpdate)s);"

for b in bigMoves:
	bIDNEW = b[0]['billID']
	try:
		actionCount = aCounts[abIDs.index(bIDNEW)]['Rows']
	except ValueError:
		actionCount = 0
	if actionCount == 0:
		for k in b:
			cursor2 = db.cursor()
			g = cursor2.execute(actINS, k) # write section characteristics to db
			cursor2.close()
			db.commit()
	if actionCount < len(b): # actionCount is currently in db, b is the count of newly processed actions
		cursor1 = db.cursor(MySQLdb.cursors.DictCursor)
		z = cursor1.execute('SELECT * FROM actions WHERE billid = %s', (bIDNEW))
		actionsCurr = list(cursor1.fetchall())
		cursor1.close()
		completeActions = actionsCurr + b[actionCount:]
		cursor3 = db.cursor()
		z = cursor3.execute('DELETE FROM actions WHERE billid = %s', (bIDNEW))
		cursor3.close()
		db.commit()
		for k in b:
			if 'manUpdate' not in k:
				k['manUpdate'] = 0
			cursor4 = db.cursor()
			g = cursor4.execute(actINS2, k) # write section characteristics to db
			cursor4.close()
		db.commit()

# counting sub-actions
print "Organizing Sub-Actions"

cursor0 = db.cursor(MySQLdb.cursors.DictCursor)
z = cursor0.execute("SELECT * FROM `actions` WHERE `status` LIKE 'COMM' and `cong` = %s",congt)
badA = cursor0.fetchall()
cursor0.close()
badA2 = sorted(badA, key=itemgetter('actionID')) 
a = badA2[0]
bid = 0
date = datetime.date(1973, 1, 1)
for a in badA2:
	if a['billID'] == bid and a['acted_at']==date:
		inc += 1
		a['subbill'] = inc
		#print 'MATCH'
	else:
		inc = 0
		a['subbill'] = inc
		bid = a['billID']
		date = a['acted_at']
		#print "NOPE"

badUPD = "UPDATE actions SET subbill = %(subbill)s WHERE actionID = %(actionID)s"
for a in badA2:
	if a['subbill'] != 0:
		cursor1 = db.cursor()
		z = cursor1.execute(badUPD,a)
		cursor1.close()
	else:
		pass

db.commit()



db.close()

print "Done with " + str(cong)